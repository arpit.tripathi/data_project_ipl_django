from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.shortcuts import render
from django.views.decorators.cache import cache_page

from . import data

# Create your views here.

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@cache_page(CACHE_TTL)
def part1(request):
    years, matches_count = data.match_played_per_year()
    print(years)
    print(matches_count)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Matches Played Per Year'},
        'xAxis': {'categories': years},
        'series': [{'name': 'Year', 'data': matches_count, 'color': 'mediumseagreen'}]
    }
    return render(request, 'dp_ipl/plot_graph.html', context={'chart': chart})


@cache_page(CACHE_TTL)
def part2(request):
    wins_per_season, years = data.get_wins_per_season()
    print(wins_per_season)
    print(years)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Matches win per year'},
        'plotOptions': {'series': {'stacking': 'normal'}},
        'xAxis': {'categories': years, 'name': 'Season'},
        'series': [{'name': name, 'data': value} for name, value in wins_per_season.items()]
    }
    return render(request, 'dp_ipl/plot_graph.html', context={'chart': chart})


@cache_page(CACHE_TTL)
def part3(request):
    dict_total_extra_runs = data.extra_runs_per_team('2016')
    print(dict_total_extra_runs)
    teams = []
    runs = []
    for key in sorted(dict_total_extra_runs.keys(), key=dict_total_extra_runs.get, reverse=True):
        teams.append(key)
        runs.append(dict_total_extra_runs[key])
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Extra Runs in 2016'},
        'xAxis': {'categories': teams},
        'series': [{'name': 'extra runs', 'data': runs, 'color': 'red'}]
    }

    return render(request, 'dp_ipl/plot_graph.html', context={'chart': chart})


@cache_page(CACHE_TTL)
def part4(request):
    dict_eco_bowlers = data.economical_bowler('2015')
    bowler = []
    economics = []
    for key in sorted(dict_eco_bowlers.keys(), key=dict_eco_bowlers.get):
        bowler.append(key)
        economics.append(dict_eco_bowlers[key])
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Economies of bowlers in 2015'},
        'xAxis': {'categories': bowler[:5]},
        'series': [{'name': 'economy', 'data': economics[:5], 'color': 'red'}]
    }
    return render(request, 'dp_ipl/plot_graph.html', context={'chart': chart})


@cache_page(CACHE_TTL)
def part5(request):
    most_runs_batsmen_dict = data.most_runs('2015')
    print(most_runs_batsmen_dict)
    batsmen = []
    runs = []

    for key in sorted(most_runs_batsmen_dict.keys(), key=most_runs_batsmen_dict.get):
        batsmen.append(key)
        runs.append(most_runs_batsmen_dict[key])
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Most Runs by Batsmen in 2015'},
        'xAxis': {'categories': batsmen[:-5:-1]},
        'series': [{'name': 'runs', 'data': runs[:-5:-1], 'color': 'red'}]
    }
    return render(request, 'dp_ipl/plot_graph.html', context={'chart': chart})
