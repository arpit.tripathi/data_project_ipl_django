import mysql.connector
from django.core.management.base import BaseCommand
from mysql.connector import Error


class Command(BaseCommand):
    help = 'Loads the csv file into mysql database'

    def handle(self, *args, **kwargs):
        try:
            mydb = mysql.connector.connect(host='blogdb.cjehqavsrgjm.us-east-2.rds.amazonaws.com',
                                           database='my_db',
                                           user='dbuser',
                                           password='password')
            mycursor = mydb.cursor()

            query_to_transfer_matches_to_mysql = "LOAD DATA LOCAL INFILE " \
                                                 "'matches.csv' " \
                                                 "INTO TABLE matches FIELDS TERMINATED BY ','" \
                                                 "  IGNORE 1 LINES;"
            mycursor.execute(query_to_transfer_matches_to_mysql)

            query_to_transfer_deliveries_to_mysql = "LOAD DATA LOCAL INFILE '" \
                                                    "deliveries.csv' INTO TABLE deliveries " \
                                                    "FIELDS TERMINATED BY ','  IGNORE 1 LINES(match_id,inning," \
                                                    "batting_team,bowling_team,over,ball,batsman,non_striker,bowler," \
                                                    "is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs," \
                                                    "penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed" \
                                                    ",dismissal_kind,fielder);"

            mycursor.execute(query_to_transfer_deliveries_to_mysql)

            mycursor.close()
            mydb.close()
        except Error as e:
            print(e)

        finally:
            mydb.close()
if __name__ == '__main__':
    Command();