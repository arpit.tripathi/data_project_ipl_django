from django.db.models import Count, Sum

from .models import Matches, Deliveries


def match_played_per_year():
    matches_played_count = Matches.objects.values('season').annotate(dcount=Count('season')) \
        .order_by('season')
    print(matches_played_count)
    years = []
    matches_count = []
    for data in matches_played_count:
        years.append(data['season'])
        matches_count.append(data['dcount'])
    return years, matches_count


def get_wins_per_season():
    wins_per_season = Matches.objects.exclude(winner__isnull=True).exclude(winner__exact='').values('winner',
                                                                                                    'season').annotate(
        wins=Count('winner')).order_by('season')
    years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
    winning_per_year = {}
    for i in list(wins_per_season):
        if i['winner'] not in winning_per_year:
            winning_per_year[i['winner']] = [0] * 10
            winning_per_year[i['winner']][years.index(int(i['season']))] = i['wins']
        else:
            winning_per_year[i['winner']][years.index(int(i['season']))] = i['wins']
    return winning_per_year, years


def extra_runs_per_team(year):
    extra_runs = Deliveries.objects.filter(match_id__season=year).values('bowling_team').annotate(sum=Sum('extra_runs'))
    print(extra_runs)
    extra_runs_with_team_dict = {}
    for data in extra_runs:
        extra_runs_with_team_dict.update({data['bowling_team']: data['sum']})
    return extra_runs_with_team_dict


def economical_bowler(year):
    economical_bowlers = Deliveries.objects.filter(match_id__season=year).values('bowler').annotate(
        sum_runs=Sum('total_runs'), count_balls=Count('bowler'))
    print(economical_bowlers)
    eco_bowlers_dict = {}
    for data in economical_bowlers:
        eco_scores = data['sum_runs'] / (data['count_balls'] / 6)
        eco_bowlers_dict.update({data['bowler']: eco_scores})
        print(eco_scores)
    print(eco_bowlers_dict)
    return eco_bowlers_dict


def most_runs(year):
    most_runs_batsmen = Deliveries.objects.filter(match_id__season=year).values('batsman').annotate(
        sum_runs=Sum('total_runs'))
    print(most_runs_batsmen)
    most_runs_batsmen_dict = {}
    for data in most_runs_batsmen:
        most_runs_batsmen_dict.update({data['batsman']: data['sum_runs']})
    return most_runs_batsmen_dict
